package ru.tsc.panteleev.tm.command.system;

import ru.tsc.panteleev.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandsCommand extends AbstractSystemCommand {

    public static final String NAME = "commands";

    public static final String DESCRIPTION = "Show commands list.";

    public static final String ARGUMENT = "-cmd";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (AbstractCommand command : commands) {
            final String name = command.getName();
            if (name != null && !name.isEmpty())
                System.out.println(name);
        }
    }

}
