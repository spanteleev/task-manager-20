package ru.tsc.panteleev.tm.command.project;

import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = "project-list";

    public static final String DESCRIPTION = "Show project list.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT TYPE:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        renderProjects(projects);
    }

}
