package ru.tsc.panteleev.tm.command.user;

import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-update-profile";

    public static final String DESCRIPTION = "Update user profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        final String userId = getAuthService().getUser().getId();
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
