package ru.tsc.panteleev.tm.command.project;

import ru.tsc.panteleev.tm.util.TerminalUtil;

import java.util.Date;

public class ProjectClearCommand extends AbstractProjectCommand {

    public static final String NAME = "project-clear";

    public static final String DESCRIPTION = "Remove all projects.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        final String userId = getUserId();
        getProjectService().clear(userId);
    }

}
