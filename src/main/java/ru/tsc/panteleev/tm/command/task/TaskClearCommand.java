package ru.tsc.panteleev.tm.command.task;

import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskClearCommand extends AbstractTaskCommand {

    public static final String NAME = "task-clear";

    public static final String DESCRIPTION = "Remove all tasks.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        final String userId = getUserId();
        getTaskService().clear(userId);
    }

}
