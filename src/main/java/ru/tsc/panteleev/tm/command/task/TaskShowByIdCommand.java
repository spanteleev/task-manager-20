package ru.tsc.panteleev.tm.command.task;

import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-id";

    public static final String DESCRIPTION = "Display task by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Task task = getTaskService().findById(userId, id);
        showTask(task);
    }

}
