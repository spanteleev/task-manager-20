package ru.tsc.panteleev.tm.command.system;

public class AboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";

    public static final String DESCRIPTION = "Show developer info.";

    public static final String ARGUMENT = "-arg";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("Name: Sergey Panteleev");
        System.out.println("E-mail: spanteleev@t1-consulting.ru");
    }

}
