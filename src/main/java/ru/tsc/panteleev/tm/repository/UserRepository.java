package ru.tsc.panteleev.tm.repository;

import ru.tsc.panteleev.tm.api.repository.IUserRepository;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.util.HashUtil;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(String login, String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    public User findByLogin(final String login) {
        for (User user : models) {
            if (login.equals(user.getLogin()))
                return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (User user : models) {
            if (email.equals(user.getEmail()))
                return user;
        }
        return null;
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}
