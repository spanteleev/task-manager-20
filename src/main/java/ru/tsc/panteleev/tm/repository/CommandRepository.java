package ru.tsc.panteleev.tm.repository;

import ru.tsc.panteleev.tm.api.repository.ICommandRepository;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class CommandRepository implements ICommandRepository {

    private final Map<String,AbstractCommand> mapByName = new LinkedHashMap<>();

    private final Map<String,AbstractCommand> mapByArgument = new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        final String name = command.getName();
        if (name != null && !name.isEmpty())
            mapByName.put(name,command);
        final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty())
            mapByArgument.put(argument,command);
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return mapByName.get(name);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        return mapByArgument.get(argument);
    }

}
