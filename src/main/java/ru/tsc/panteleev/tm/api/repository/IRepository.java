package ru.tsc.panteleev.tm.api.repository;

import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    List<M> findAll(Sort sort);

    M findById(String id);

    M findByIndex(Integer index);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    void removeAll(Collection<M> collection);

    void clear();

    boolean existsById(String id);

    int getSize();

}
