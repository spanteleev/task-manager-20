package ru.tsc.panteleev.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ILoggerService getLoggerService();

    IUserService getUserService();

    IAuthService getAuthService();

}
