package ru.tsc.panteleev.tm.api.service;

import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

}
