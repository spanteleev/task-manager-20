package ru.tsc.panteleev.tm.api.repository;

import ru.tsc.panteleev.tm.model.User;

public interface IUserRepository extends IRepository<User>{

    User create(String login, String password);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

}
