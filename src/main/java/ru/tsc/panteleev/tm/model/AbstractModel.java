package ru.tsc.panteleev.tm.model;

import java.util.UUID;

public class AbstractModel {

    private String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
