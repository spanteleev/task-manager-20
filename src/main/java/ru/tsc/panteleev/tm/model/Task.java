package ru.tsc.panteleev.tm.model;

import ru.tsc.panteleev.tm.api.model.IWBS;
import ru.tsc.panteleev.tm.enumerated.Status;
import java.util.Date;

public final class Task extends AbstractUserOwnedModel implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private String projectId = null;

    private Date created = new Date();

    private Date dateBegin;

    private Date dateEnd;

    public Task() {
    }

    public Task(String name, Status status, Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    @Override
    public String toString() {
        return name + " : " + description + " : " + status.getDisplayName() + " : " + dateBegin;
    }

}
