package ru.tsc.panteleev.tm.service;

import ru.tsc.panteleev.tm.api.repository.IRepository;
import ru.tsc.panteleev.tm.api.service.IService;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.panteleev.tm.exception.field.IdEmptyException;
import ru.tsc.panteleev.tm.exception.field.IndexIncorrectException;
import ru.tsc.panteleev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public M findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Override
    public M findByIndex(final Integer index) {
        if (index == null || index<0) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public M remove(final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index<0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }
}
