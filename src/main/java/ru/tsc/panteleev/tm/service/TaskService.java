package ru.tsc.panteleev.tm.service;

import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.panteleev.tm.api.service.ITaskService;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.model.Task;
import java.util.Date;
import java.util.List;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task create(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Task create(final String userId, String name, String description, Date dateBegin, Date dateEnd) {
        final Task task = create(userId, name, description);
        if (task == null) throw new TaskNotFoundException();
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index<0) throw new IndexIncorrectException();
        final Task task = findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (index == null || index<0) throw new IndexIncorrectException();
        final Task task = findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        if (status == null)  throw new StatusIncorrectException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        if (status == null)  throw new StatusIncorrectException();
        task.setStatus(status);
        return task;
    }

}
