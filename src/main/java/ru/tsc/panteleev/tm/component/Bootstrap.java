package ru.tsc.panteleev.tm.component;

import ru.tsc.panteleev.tm.api.repository.ICommandRepository;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.repository.IUserRepository;
import ru.tsc.panteleev.tm.api.service.*;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.command.project.*;
import ru.tsc.panteleev.tm.command.system.*;
import ru.tsc.panteleev.tm.command.task.*;
import ru.tsc.panteleev.tm.command.user.*;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.panteleev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.repository.CommandRepository;
import ru.tsc.panteleev.tm.repository.ProjectRepository;
import ru.tsc.panteleev.tm.repository.TaskRepository;
import ru.tsc.panteleev.tm.repository.UserRepository;
import ru.tsc.panteleev.tm.service.*;
import ru.tsc.panteleev.tm.util.DateUtil;
import ru.tsc.panteleev.tm.util.TerminalUtil;

import java.util.List;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new HelpCommand());
        registry(new AboutCommand());
        registry(new CommandsCommand());
        registry(new ExitCommand());
        registry(new TaskListCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowListByProjectIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new ProjectListCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new UserRegistrationCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserShowProfileCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserChangePasswordCommand());
    }

    private void registry(AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }
    
    private void initData() {
        userService.create("test", "test", "qwerty@qwerty.ru");
        userService.create("admin", "admin", Role.ADMIN);
        final String adminId = userService.findByLogin("admin").getId();
        final String testId = userService.findByLogin("test").getId();
        projectService.create(adminId,"p3","333");
        projectService.create(adminId,"p1","111");
        projectService.create(testId,"p5","555");
        projectService.create(testId,"p4","444");
        projectService.create(testId,"p6","666");
        taskService.create(adminId,"t3", "333333");
        taskService.create(adminId,"t1", "111111");
        taskService.create(testId,"t5", "555555");
        taskService.create(testId,"t4", "444444");
        taskService.create(testId,"t6", "666666");
    }

    public void run(String[] args) {
        if (runWithArgument(args))
            System.exit(0);
        initData();
        initLogger();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                String command = TerminalUtil.nextLine();
                runWithCommand(command);
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
            }
        }
    }

    public void runWithCommand(String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public boolean runWithArgument(String[] args) {
        if (args == null || args.length == 0) return false;
        runWithArgument(args[0]);
        return true;
    }

    public void runWithArgument(String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(argument);
        if (abstractCommand == null)
            throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

}
